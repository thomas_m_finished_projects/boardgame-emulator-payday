# **PayDay Emulator**

## **Overview**
This project is a digital implementation of the classic board game **PayDay**, created as part of the **PHYS30762 Object-Oriented C++** module coursework at The University of Manchester. The game supports 2-4 players and replicates the gameplay of PayDay, where players aim to achieve the highest net worth after navigating a calendar-based board over one or more months.

The emulator utilizes advanced C++ features such as object-oriented programming, file handling, and randomization to simulate dice rolls, shuffle card decks, and manage player interactions. The game is entirely text-based and features an interactive console interface.

---

## **Features**
- **Full PayDay Simulation**:
  - Dice rolling, card drawing, and space-specific actions.
  - Supports key PayDay mechanics like sweepstakes, lotteries, and buying/selling deals.
- **Dynamic Game Board**:
  - Tracks player positions and displays the board after each turn.
- **Card Management**:
  - Deal and mail cards are stored in `.dat` files, shuffled, and accessed dynamically during gameplay.
- **Leaderboards**:
  - Stores player scores in a persistent `Leaderboard.dat` file for future reference.
- **Error Handling**:
  - Includes robust error-handling mechanisms for invalid inputs and edge cases.

---

## **Repository Structure**

Boardgame Emulator Payday/
│
├── src/                    # Source code
│   ├── Board.cpp           # Implements board logic and player data management
│   ├── Board.h
│   ├── Cards.cpp           # Card management for mail and deal cards
│   ├── Cards.h
│   ├── Spaces.cpp          # Logic for various board spaces (e.g., sweepstakes, lottery)
│   ├── Spaces.h
│   ├── Game.cpp            # Entry point of the program (main function)
│   ├── Payday.cpp          # Implements the "PayDay" space logic
│   ├── Player.cpp          # Functions for modifying player data
│   ├── InitialInput.cpp    # Handles player and game setup inputs
│
├── docs/                   # Documentation
│   └── Project Report.pdf  # Detailed project report (design, implementation, results)
│
├── build/                  # Executable file (requires data files in same directory)
│   ├── PayDayEmulator.exe
│   ├── DealCards.dat       # Contains deal card descriptions and values
│   ├── MailCards.dat       # Contains mail card descriptions and actions
│   ├── Leaderboard.dat     # Stores high scores and winners
│
└── README.md               # Project documentation

## **Getting Started**

### **Prerequisites**
- A C++ compiler (e.g., GCC, Clang, or Visual Studio).
- Standard libraries such as `iostream`, `fstream`, and STL components.

### **Build and Run**

#### On Linux/macOS:
1. Navigate to the `src` folder.
2. Compile the program:
   ```bash
   g++ -o PayDayEmulator *.cpp -I ./
   ```
3. Run the program:
   ```
   ./PayDayEmulator
   ```
#### On Windows:
1. Open the solution in Visual Studio.
2. Build the project:
   - In Visual Studio, press `Ctrl + Shift + B` to build the project.
3. Run the program:
   - Press `Ctrl + F5` to run the program without debugging.


### **Using the Prebuilt Executable**
- Navigate to the `build/` directory.
- Ensure the following data files are in the same directory as `PayDayEmulator.exe`:
  - `DealCards.dat`
  - `MailCards.dat`
  - `Leaderboard.dat`
- Double-click `PayDayEmulator.exe` to launch the game.

---

## **Gameplay**
1. **Setup**:
   - Specify the number of players (2-4).
   - Choose the number of months to play.

2. **Objective**:
   - Roll dice, move on the board, and interact with spaces.
   - Collect deals, manage loans, and increase your net worth.

3. **End Game**:
   - The player(s) with the highest net worth win.
   - Winners can add their name to the persistent leaderboard.

---

## **Documentation**
- The detailed project report is available in the `docs/` folder as `Report.pdf`.
- It includes:
  - Code design and structure.
  - Features implemented and challenges faced.
  - Results and future improvements.

---

## **Future Improvements**
- Implement a **Graphical User Interface (GUI)** to replace the text-based interaction.
- Enhance efficiency by replacing raw pointers with smart pointers (e.g., `std::shared_ptr`).
- Expand functionality for online multiplayer.

---

## **Credits**
- **Author**: Thomas Mallinson
- Developed as coursework for PHYS30762 at The University of Manchester.

