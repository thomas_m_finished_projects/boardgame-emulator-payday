#include "Board.h"
#include <iostream>
#include <vector>
#include <string>

/*!
 * \brief Contains functions to modify player/board/deal data.
 */

void board::output_player_information(int i)
{
	std::cout << player_name[i] << " player:\nCash = $" << cash[i] << ",\nLoan = $"
		<< loan[i] << ",\nBills = $" << bills[i] << "." << std::endl;
}

void board::change_loan(int i, double loan_change)
{
	loan[i] = loan[i] + loan_change;
}
void board::change_cash(int i, double cash_change)
{
	cash[i] = cash[i] + cash_change;
}

void board::change_jackpot(double jackpot_change)
{
	jackpot = jackpot + jackpot_change;
}

void board::change_bills(int i, double bills_change)
{
	bills[i] = bills[i] + bills_change;
}

void board::advance_a_month(int i)
{
	months[i] = months[i] + 1;
	position[i] = 1;
}

void board::loan_check_and_take_out(int i)
{
	if (cash[i] < 0) {
		std::cout << "You're in debt $";
		double temp{ -cash[i] };
		int debt{}; debt = static_cast<int>(temp);
		std::cout << debt << ".\nYou need to take out a loan." << std::endl;
		int remainder{ debt % 1000 };
		int withdrawing_loan_amount{};
		if (remainder == 0) { withdrawing_loan_amount = 1000 * (debt / 1000); }
		else {
			int debt_minus_remainder{ debt - remainder };
			withdrawing_loan_amount = ((debt_minus_remainder / 1000) + 1) * 1000;
		}
		loan[i] = loan[i] + withdrawing_loan_amount;
		cash[i] = cash[i] + withdrawing_loan_amount;
		std::cout << "You take out a loan of $" << withdrawing_loan_amount << ".\nYou now have $" << cash[i] << " of cash."
			"\nYour current loan is $" << loan[i] << "." << std::endl;
	}
}

void board::manually_change_position(int i, int new_position)
{
	position[i] = new_position;
}

void board::change_position(int i)
{
	std::cout << player_name[i] << " player, please press enter to roll your dice:" << std::endl;
	std::cin.ignore();
	std::cin.get();
	int dice_roll{};
	int min{ 1 };
	int max{ 6 };
	dice_roll = rand() % (max - min + 1) + min;
	std::cout << " _____ \n|     |\n|  " << dice_roll << "  |\n|_____|" << std::endl;
	position[i] = dice_roll + position[i];

	if (dice_roll == 6) {
		std::cout << "Congratulations! You rolled a 6!!!\nYou win the jackpot!!!" << std::endl;
		change_cash(i,-jackpot);
		change_jackpot(-jackpot);
	}
}