/*!
 * \author Thomas Mallinson
 *
 * \brief This is where the main method resides
 * 
 * This project is based upon the board game PAY DAY.
 * This project is for the PHYS30762 course in Object Oriented C++.
 * This is a digital playable game for 2-4 players.
 * 
 */

#include "Spaces.h"
#include "Board.h"
#include "Cards.h"
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <utility>
#include <map>
#include <fstream>
#include <tuple>

int main()
{
    // Generates a seed based on current time to make random number generation different
    // each time the program runs.
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    int number_of_players{};
    number_of_players = input_number_of_players();
    int number_of_months_input{};
    number_of_months_input = input_number_of_months();
    // Creates and defines vectors used to initialise an object of the board class.
    std::vector<size_t> position;
    std::vector<int> player_number;
    std::vector<std::string> player_name;
    std::vector<double> player_cash;
    std::vector<double> player_loan;
    std::vector<double> player_bills;
    std::vector<int> player_months;
    for (int i{}; i < number_of_players; i++) {
        if (i == 0) {
            position.push_back(1); player_number.push_back(1); player_name.push_back("Red");
            player_cash.push_back(3500); player_loan.push_back(0); player_bills.push_back(0); player_months.push_back(0);
        }
        if (i == 1) {
            position.push_back(1); player_number.push_back(2); player_name.push_back("Blue");
            player_cash.push_back(3500); player_loan.push_back(0); player_bills.push_back(0); player_months.push_back(0);
        }
        if (i == 2) {
            position.push_back(1); player_number.push_back(3); player_name.push_back("Green");
            player_cash.push_back(3500); player_loan.push_back(0); player_bills.push_back(0); player_months.push_back(0);
        }
        if (i == 3) {
            position.push_back(1); player_number.push_back(4); player_name.push_back("Yellow");
            player_cash.push_back(3500); player_loan.push_back(0); player_bills.push_back(0); player_months.push_back(0);
        }
    }
    // Creates vectors used to initialise an object of the got_deal class.
    std::vector<std::string> initialise_desc; std::vector<int> initialise_costs;
    std::vector<int> initialise_values; std::vector<int> initialise_ownership;

    board board_1{ 0, 31,number_of_players, position, player_number, player_name, player_cash, player_loan, player_bills,
        player_months, 0, initialise_desc,initialise_costs,initialise_values,initialise_ownership,0,number_of_months_input };
    board* board_pointer;
    board_pointer = &board_1;

    // Initialises objects corresponding to each space on the board.
    got_deal deal_1{ board_pointer, 0 };
    found_a_buyer buyer_found_1{ board_pointer,0};
    got_mail mail_1{ board_pointer,1,1 };
    sweepstakes sweep_2{ board_pointer,2 };
    got_mail mail_3{ board_pointer,3,2 };
    got_mail mail_5{ board_pointer,5,3 };
    lottery lottery_6{ board_pointer,6 };
    sweet_sunday sweet_7{ board_pointer,7 };
    radio_contest radio_8{ board_pointer,8 };
    happy_birthday birthday_10{ board_pointer,10 };
    got_mail mail_11{ board_pointer,11,1 };
    fun_day fun_day_1{ board_pointer,14 };
    got_mail mail_16{ board_pointer,16,3 };
    buy_groceries groceries_1{ board_pointer,18 };
    got_mail mail_19{ board_pointer,19,1 };
    lottery lottery_20{ board_pointer, 20 };
    special_deal special_deal_21{ board_pointer,21 };
    got_mail mail_22{ board_pointer,22,1 };
    got_mail mail_24{ board_pointer,24,2 };
    date_night_at_the_casino date_night_1{ board_pointer,26 };
    walk_for_charity charity_walk_1{ board_pointer, 28 };
    payday_space payday_1{ board_pointer,31 };
    // Creates two vectors of integers and randomises them to use as references to objects
    // in the deal and mail maps.
    std::vector<int> deal_reference{};
    std::vector<int> mail_reference{};
    for (int i{1}; i < 36; i++) {
        mail_reference.push_back(i);
        if (i < 18) { deal_reference.push_back(i); }
    }
    std::random_shuffle(deal_reference.begin(), deal_reference.end());
    std::random_shuffle(mail_reference.begin(), mail_reference.end());
    // Initialises maps for deal cards and mail cards
    deal_cards deal;
    mail_cards mail;
    std::string deal_cards_filename{ "DealCards.dat" };
    std::string mail_cards_filename{ "MailCards.dat" };
    std::vector<std::string>deal_cards_vector;
    std::vector<std::string>mail_cards_vector;
    read_from_file(deal_cards_vector, deal_cards_filename);
    read_from_file(mail_cards_vector, mail_cards_filename);
    for (int i{ 1 }; i < 36; i++) {
        mail[i] = mail_cards_vector[i-1];
        if (i < 18) { deal[i] = deal_cards_vector[i-1]; }
    }
    deal_cards_vector.clear();
    mail_cards_vector.clear();
    int is_game_over{ 1 };
    std::cout << board_1 << std::endl;
    /*
    * A loop to carry out turns of the game payday.
    * Changes the position of a player, then carries out an action corresponding to the
    * space they move to.
    * Repeats, cycling through each player, until all players have reached the payday space
    * on the final month of play.
    */
    while (is_game_over == 1) {
        for (int a{}; a < number_of_players; a++) {
            std::cout << "\n" << std::endl;
            if (board_1.get_player_months(a) != number_of_months_input) {
                board_1.change_position(a);
                if (board_1.get_player_position(a) > 31) { board_1.manually_change_position(a, 31); }
                std::cout << board_1 << std::endl;
                board_1.output_player_information(a);
                if (board_1.get_player_position(a) == 2) { sweep_2.land_action(a); }
                if (board_1.get_player_position(a) == 6) { lottery_6.land_action(a); }
                if (board_1.get_player_position(a) == 7) { sweet_7.land_action(a); }
                if (board_1.get_player_position(a) == 8) { radio_8.land_action(a); }
                if (board_1.get_player_position(a) == 10) { birthday_10.land_action(a); }
                if (board_1.get_player_position(a) == 14) { fun_day_1.land_action(a); }
                if (board_1.get_player_position(a) == 18) { groceries_1.land_action(a); }
                if (board_1.get_player_position(a) == 20) { lottery_20.land_action(a); }
                if (board_1.get_player_position(a) == 21) {
                    if (board_1.get_deal_card_counter() == 17) {
                        // Reshuffles and resets the counter and reference vector.
                        // Effectively re-shuffling the deck of cards once all cards have been taken.
                        board_1.reset_deal_card_counter();
                        std::random_shuffle(deal_reference.begin(), deal_reference.end());
                    }
                    // Initialises a tuple based on an element from a map corresponding to a key.
                    int reference{ deal_reference[board_1.get_deal_card_counter()] };
                    auto deal_card_tuple21 = get_deal_card_information(deal, reference);
                    special_deal_21.card_land_action(a, std::get<0>(deal_card_tuple21),
                        std::get<1>(deal_card_tuple21), std::get<2>(deal_card_tuple21));
                }
                if (board_1.get_player_position(a) == 23) { buyer_found_1.land_action(a); }
                if (board_1.get_player_position(a) == 26) { date_night_1.land_action(a); }
                if (board_1.get_player_position(a) == 28) { charity_walk_1.land_action(a); }
                if (board_1.get_player_position(a) >= 31) {
                    payday_1.land_action(a);
                    // The player is manually moved back to the start of the board and recieves a letter.
                    if (board_1.get_player_months(a) != number_of_months_input) {
                        if (mail_1.get_mail_card_counter() == 35) {
                            mail_1.reset_mail_card_counter();
                            std::random_shuffle(mail_reference.begin(), mail_reference.end());
                        }
                        std::cout << "You have recieved a letter" << std::endl;
                        int reference{ mail_reference[mail_1.get_mail_card_counter()] };
                        auto mail_card_tuple1 = get_mail_card_information(mail, reference);
                        mail_1.card_land_action(a, std::get<0>(mail_card_tuple1),
                            std::get<1>(mail_card_tuple1), 0);
                    }
                } else {
                    std::vector<got_mail> mail_vector{ mail_1,mail_3,mail_5,mail_11,mail_16,mail_19,mail_22,mail_24 };
                    std::vector<int> deal_stepper{ 4,12,15,25 };
                    std::vector<int> buyer_stepper{ 9,13,17,23,27,29,30 };
                    for (auto mail_vector_it = mail_vector.begin(); mail_vector_it < mail_vector.end(); mail_vector_it++) {
                        if (board_1.get_player_position(a) == mail_vector_it->get_date()) {
                            if (mail_vector_it->get_mail_card_counter() == 35) {
                                mail_vector_it->reset_mail_card_counter();
                                std::random_shuffle(mail_reference.begin(), mail_reference.end());
                            }
                            if (mail_vector_it->get_number_of_cards() == 1) {
                                std::cout << "You have recieved a letter." << std::endl;
                            } else {
                                std::cout << "You have recieved " << mail_vector_it->get_number_of_cards() << " letters." <<
                                    std::endl;
                            }
                            for (int k{}; k < mail_vector_it->get_number_of_cards(); k++) {
                                std::cout << "\n" << std::endl;
                                if (mail_vector_it->get_mail_card_counter() == 35) {
                                    mail_vector_it->reset_mail_card_counter();
                                    std::random_shuffle(mail_reference.begin(), mail_reference.end());
                                }
                                int reference{ mail_reference[mail_vector_it->get_mail_card_counter()] };
                                auto mail_card_tuple1 = get_mail_card_information(mail, reference);
                                mail_vector_it->card_land_action(a, std::get<0>(mail_card_tuple1),
                                    std::get<1>(mail_card_tuple1), 0);
                            }
                        }
                    }
                    for (auto deal_vector_it = deal_stepper.begin(); deal_vector_it < deal_stepper.end(); deal_vector_it++) {
                        if (board_1.get_player_position(a) == *deal_vector_it) {
                            if (board_1.get_deal_card_counter() == 17) {
                                board_1.reset_deal_card_counter();
                                std::random_shuffle(deal_reference.begin(), deal_reference.end());
                            }
                            int reference{ deal_reference[board_1.get_deal_card_counter()] };
                            auto deal_card_tuple1 = get_deal_card_information(deal, reference);
                            deal_1.card_land_action(a, std::get<0>(deal_card_tuple1), std::get<1>(deal_card_tuple1),
                                std::get<2>(deal_card_tuple1));
                        }
                    }
                    for (auto buyer_vector_it = buyer_stepper.begin(); buyer_vector_it < buyer_stepper.end();
                        buyer_vector_it++) {
                        if (board_1.get_player_position(a) == *buyer_vector_it) {
                            buyer_found_1.land_action(a);
                        }
                    }
                }
                // Checks if players have a positive cash value at the end of their turn and takes out a loan if they dont.
                if (board_1.get_player_months(a) != number_of_months_input) { board_1.loan_check_and_take_out(a); }
            }
        }
        // Checks if all players have cycled through the total number of months. Ends the while loop once they have.
        int all_at_payday{ 0 };
        for (int w{}; w < board_1.get_number_of_players(); w++) {
            if (board_1.get_player_months(w) == number_of_months_input) { all_at_payday++; }
        }
        if (all_at_payday == board_1.get_number_of_players()) {
            is_game_over = 0;
        }
    }
    std::cout << "All players have completed the final month of play.\nThe player with the highest net worth\n"
        "(the most cash, or the least in debt) wins!" << std::endl;
    std::vector<double> end_game_net_worth;
    std::vector<int> is_player_winner;
    std::vector<std::string> player_names;
    for (int i{}; i < board_1.get_number_of_players(); i++) {
        end_game_net_worth.push_back(board_1.get_player_cash(i));
        player_names.push_back(board_1.get_player_name(i));
    }
    double max = *max_element(end_game_net_worth.begin(), end_game_net_worth.end());
    for (int i{}; i < board_1.get_number_of_players(); i++) {
        if (end_game_net_worth[i] == max) {
            is_player_winner.push_back(1);
        } else { is_player_winner.push_back(0); }
    }
    int number_of_winners{};
    // Calculates the number of winners
    for (int i{}; i < board_1.get_number_of_players(); i++) { number_of_winners = number_of_winners + is_player_winner[i]; }
    if (number_of_winners > 1) { std::cout << number_of_winners << " players have won!" << std::endl; }
    std::string leaderboard_filename{ "Leaderboard.dat" };
    std::vector<std::string>leaderboard_vector;
    // Stores the information in the leaderboard.dat file in the leaderboard_vector vector.
    read_from_file(leaderboard_vector, leaderboard_filename);
    // Outputs the leaderboard and updates the leaderboard file.
    update_and_output_leaderboard(leaderboard_vector, leaderboard_filename, player_names, is_player_winner, end_game_net_worth);
    return 0;
}