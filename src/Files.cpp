#include "Board.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <utility>

/*!
 * \brief Contains functions to write and read from files.
 * The write function also displays the leaderboard.
 */

void read_from_file(std::vector<std::string>& data_vector, std::string data_filename)
{
	// Creates a filestream and stores read data in a vector.
	std::fstream file_stream{ data_filename };
	try {
		if (file_stream.good() == false) { throw data_filename; }
	}
	catch (std::string error_file_name) { std::cerr << "Error: File " << error_file_name << "not found." << std::endl; }
	std::string line;
	while (std::getline(file_stream, line)) { data_vector.push_back(line); }
	file_stream.close();
}

void update_and_output_leaderboard(std::vector<std::string> leaderboard_vector, std::string leaderboard_filename,
	std::vector<std::string> player_names, std::vector<int> is_player_winner, std::vector<double> end_game_net_worth)
{
	std::vector<std::string> leaderboard_names;
	std::vector<double> leaderboard_net_worth;
	std::vector<std::string> winner_names;
	std::vector<double> winner_net_worth;
	for (int i{}; i < player_names.size(); i++) {
		if (is_player_winner[i] == 1) {
			std::cout << "Congratulations " << player_names[i] << " player!\nYou have won!" << std::endl;
			std::cout << "Your final net worth is $" << end_game_net_worth[i] << std::endl;
			std::cout << "Please enter the name you would like to be added to the leaderboard (don't include spaces or '_'):" << std::endl;
			int input_success{ 1 };
			std::string winner_name_input{};
			int loop_not_ending_flag{ 0 };
			// A loop to validate user input. If the loop repeats too many times, a default name will be used.
			while (input_success == 1) {
				loop_not_ending_flag = loop_not_ending_flag + 1;
				std::cin >> winner_name_input;
				if (winner_name_input.find('_') != std::string::npos) {
					std::cout << "Please do not include '_' in your name." << std::endl;
					winner_name_input = "";
				}
				else { input_success = 0; }
				if (loop_not_ending_flag == 10) {
					std::cerr << "Error: input failed 10 times. Using default name:" << std::endl;
					winner_name_input = "default";
					input_success = 0;
				}
			}
			winner_names.push_back(winner_name_input);
			winner_net_worth.push_back(end_game_net_worth[i]);
		}
	}
	// Splits the strings in each line of the leaderboard_vector vector into a string and a double, using a delimiter.
	for (int i{}; i < leaderboard_vector.size(); i++) {
		std::string output_string{ leaderboard_vector[i] };
		std::string delimiter{ "_" };
		size_t pos = 0;
		std::string token;
		while ((pos = output_string.find(delimiter)) != std::string::npos) {
			token = output_string.substr(0, pos);
			leaderboard_names.push_back(token);
			output_string.erase(0, pos + delimiter.length());
		}
		leaderboard_net_worth.push_back(stod(output_string));
	}
	// Inserts the winner's data in the correct place / order of the leaderboard.
	for (int i{}; i < winner_names.size();i++) {
		int index{0};
		for (int j{}; j < leaderboard_names.size(); j++) {
			if (winner_net_worth[i] < leaderboard_net_worth[j]) {
				index = j + 1;
			}
		}
		leaderboard_names.insert(leaderboard_names.begin() + index, winner_names[i]);
		leaderboard_net_worth.insert(leaderboard_net_worth.begin() + index, winner_net_worth[i]);
	}
	// Opens a filestream to write to the file.
	std::ofstream myfile;
	myfile.open(leaderboard_filename);
	try {
		if (myfile.good() == false) { throw leaderboard_filename; }
	}
	catch (std::string error_filename) { std::cerr << "Error: File " << error_filename << " not found." << std::endl; }
	// Outputs the leaderboard, and writes it to the file line by line.
	std::cout << "\nLeaderboard:" << std::endl;
	for (int i{}; i < leaderboard_names.size(); i++) {
		std::cout << "\n" << leaderboard_names[i] << ": $" << leaderboard_net_worth[i];
		std::ostringstream output_stream;
		output_stream << leaderboard_names[i] << "_" << leaderboard_net_worth[i]<<"\n";
		std::string output_string{ output_stream.str() };
		myfile << output_string;
		output_stream.str(""); // clears stream content
	}
	myfile.close();
}