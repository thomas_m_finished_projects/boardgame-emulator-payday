#include "Board.h"
#include <iostream>

/*!
 * \brief Contains functions to take user input for number of players and months.
 */
int input_number_of_players()
{
	int number_of_players{};
	std::cout << "How many players would like to play in the game?\n" <<
		"Minimum number = 2, maximum number = 4." << std::endl;
	try {
		std::cin >> number_of_players;
		if (std::cin.fail()) throw(0);
		if (number_of_players > 4 || number_of_players < 2) throw(1);
	}
	catch (int error_flag) {
		if (error_flag == 0) {
			std::cerr << "Error: Invalid input type.\nPlease enter an integer for number of players" << std::endl;
			return 1;
		}
		if (error_flag == 1) {
			std::cerr << "Error: Out of range error.\n" <<
				"Please enter a number from 2 to 4, inclusive, for number of players" << std::endl;
			return 1;
		}
	}
	return number_of_players;
}
int input_number_of_months()
{
	int number_of_months_input{};
	try {
		std::cout << "How many months would you like the game to cycle?" << std::endl;
		std::cin >> number_of_months_input;
		if (std::cin.fail()) throw(0);
		if (number_of_months_input < 1) throw(1);
	}
	catch (int error_flag) {
		if (error_flag == 0) {
			std::cerr << "Error: Invalid input type.\nPlease enter an integer (whole number) for number of months you would"
				" like the game to cycle." << std::endl;
			return 1;
		}
		if (error_flag == 1) {
			std::cerr << "Error: Input range error.\nPlease enter a positive number of months you would like the game to cycle."
				<< std::endl;
			return 1;
		}
	}
	return number_of_months_input;
}