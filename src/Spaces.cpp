#include "Board.h"
#include "Spaces.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

/*!
 * \brief Contains land_action functions called when a player lands on the corresponding space.
 * Also contains the function used to roll dice.
 */

int spaces::dice_roller()
{
	std::cout << "Press enter to roll a dice:" << std::endl;
	std::cin.ignore();
	std::cin.get();
	int dice_roll{};
	dice_roll = rand() % (6) + 1;
	std::cout << " _____ \n|     |\n|  " << dice_roll << "  |\n|_____|" << std::endl;
	return dice_roll;
}

void sweepstakes::land_action(int i) {
	std::cout << "Sweepstakes: Roll a dice and win $1000 * 1 roll of the die."<< std::endl;
	int dice_roll{ dice_roller() };
	std::cout << "You rolled a " << dice_roll << " , you win $" << dice_roll * 1000 << std::endl;
	board_data->change_cash(i, (dice_roll * 1000));
}

void lottery::land_action(int k) {
	std::cout << "Lottery: The bank antes up $1000. Each of you may choose to ante $100." <<
		"\nIf you choose to, you will choose a number from 1 to 6. A dice will then be rolled." <<
		"\nIf the dice roll matches your number, you will win the money.\nThe dice will be re-rolled if" <<
		" no one wins, until someone does." << std::endl;
	int ante{1000};
	int ante_count{};
	std::vector<std::string> anted_players{};
	std::vector<int> player_dice_choices{};
	int for_not_ending_flag{0};
	// Uses a loop to check user inputs and retake the input if it is invalid.
	for (int i{}; i < board_data->get_number_of_players(); i++) {
		std::cout << board_data->get_player_name(i) << " player, would you like to ante $100?" <<
			"\nPlease type 'Y' if you would or 'N' if you would prefer not to." << std::endl;
		std::string ante_choice{};
		std::cin >> ante_choice;
		for_not_ending_flag++;
		// If statement to resolve an error that can occur and cause the for statement to repeat endlessly.
		if (for_not_ending_flag == 30) {
			for_not_ending_flag = 0;
			std::cerr << "Input error: An error has occured taking your input.\nYou will not be included in the ante." << std::endl;
			anted_players.push_back("N");
			player_dice_choices.push_back(0);
		}
		if ((ante_choice != "Y") && (ante_choice != "N")) {
			i--;
			std::cout << "Please only type Y or N when asked if you would like to ante." << std::endl;
		}
		if (ante_choice == "Y") {
			std::cout << "Please choose your dice number:" << std::endl;
			int dice_choice{};
			int is_choice_unique{0};
			std::cin >> dice_choice;
			if (std::cin.fail()) {
				std::cout << "Please enter a valid integer value when asked for dice number." << std::endl;
				i--;
			}
			if ((dice_choice > 6) || (dice_choice < 1)) {
				std::cout << "Please enter a value between 1 and 6, inclusive, when asked for dice number." << std::endl;
				i--;
			} else {
				// Checks that all players have chosen different dice numbers.
				if (i != 0) {
					for (int j{}; j < i; j++) {
						if (dice_choice == player_dice_choices[j]) {
							is_choice_unique = 1;
						}
					}
				}
				if (is_choice_unique == 1) {
					std::cout << "Please enter a dice value that has not been entered by another player." << std::endl;
					i--;
				}
				if (is_choice_unique == 0) {
					anted_players.push_back("Y");
					player_dice_choices.push_back(dice_choice);
					ante = ante + 100;
					ante_count++;
				}
			}
		}
		if (ante_choice == "N") {
			anted_players.push_back("N");
			player_dice_choices.push_back(0);
		}
	}
	int roll_match{1};
	if (ante_count != 0) {
		int loop_not_ending_flag{0};
		while (roll_match == 1) {
			loop_not_ending_flag = loop_not_ending_flag + 1;
			int roll{ dice_roller() };
			for (int i{}; i < board_data->get_number_of_players(); i++) {
				if (anted_players[i] == "Y") {
					if (player_dice_choices[i] == roll) {
						std::cout << "Congratulations " << board_data->get_player_name(i) << " player, you have won the ante: $"
							<< ante << "." << std::endl;
						roll_match = 0;
						board_data->change_cash(i, ante);
					}
				}
			}
			if (loop_not_ending_flag == 20) {
				roll_match = 1;
				std::cerr << "Roll match error:\nA winner was unable to be found so no one wins the ante." << std::endl;
			}
		}
	} else { std::cout << "No one anted, so the bank keeps the money" << std::endl; }
}

void sweet_sunday::land_action(int i) {
	std::cout << "Sweet sunday: " << board_data->get_player_name(i) <<
		" player, it's Sunday, you decide to take the day off to rest." << std::endl;
}

void radio_contest::land_action(int k) {
	std::cout << "Radio Contest: Your local radio station is holding a contest.\nEach of you will roll a die." <<
		"\nThe player with the highest roll wins $1000.\nIn the case of a tie, the tying players roll again." << std::endl;
	int tie_counter{-1};
	std::vector<int>include_in_contest{};
	for (int i{}; i < board_data->get_number_of_players(); i++) {
		include_in_contest.push_back(0);
	}
	// Rolls dice until the highest dice rolled is only rolled by 1 player.
	int loop_not_ending_flag{0};
	while (tie_counter != 0) {
		loop_not_ending_flag = loop_not_ending_flag + 1;
		tie_counter = -1;
		std::vector<int>radio_contest_rolls{};
		for (int i{}; i < board_data->get_number_of_players(); i++) {
			if (include_in_contest[i] == 0) {
				std::cout << board_data->get_player_name(i) << " player: " << std::endl;
				radio_contest_rolls.push_back(dice_roller());
			} else { radio_contest_rolls.push_back(0); }
		}
		for (int i{}; i < board_data->get_number_of_players(); i++) {
			if (radio_contest_rolls[i] == *max_element(radio_contest_rolls.begin(), radio_contest_rolls.end())) {
				tie_counter++;
			} else { include_in_contest[i] = 1; }
		}
		if (tie_counter == 0) {
			for (int i{}; i < board_data->get_number_of_players(); i++) {
				if (radio_contest_rolls[i] == *max_element(radio_contest_rolls.begin(), radio_contest_rolls.end())) {
					std::cout << "Congratulations " << board_data->get_player_name(i) << " player." <<
						"\nYou win $1000!" << std::endl;
					board_data->change_cash(i, 1000);
				}
			}
		} else { std::cout << "There is a tie. Tying players will now reroll:" << std::endl; }
		// After 20 dice rolls, if there has been no winner, the following code will end the loop.
		if (loop_not_ending_flag == 20) {
			tie_counter = 0;
			std::cerr << "Tie counter error: A winner was unable to be found.\nNo one wins the money" << std::endl;
		}
	}
}

void happy_birthday::land_action(int i) {
	std::cout << "Happy Birthday: It's your birthday!\nAll your generous friends (the other players) give you $100 each."
		"\nWhat generous friends you have!" << std::endl;
	for (int j{}; j < board_data->get_number_of_players(); j++) {
		if (i == j) {
			board_data->change_cash(i, ((board_data->get_number_of_players() - 1) * 100));
		} else { board_data->change_cash(j, -100); }
	}
}

void fun_day::land_action(int i) {
	std::cout << "Fun Day: Wow, what a fun day you had!\nYou went to the fairground and spent $200 on tickets for the rides"
		".\nYou place $200 in the jackpot." << std::endl;
	board_data->change_cash(i, -200);
	board_data->change_jackpot(200);
}

void buy_groceries::land_action(int i) {
	std::cout << "Groceries: It costs money to feed a hungry family.\n"
		"You spend $200 on groceries, which is sent to the jackpot." << std::endl;
	board_data->change_cash(i, -200);
	board_data->change_jackpot(200);
}

void date_night_at_the_casino::land_action(int i) {
	std::cout << "Date night at the casino: You may be lucky, you may not be.\n"
		"Roll a die, if you roll an odd number, you gain $500.\nIf you roll an even number,"
		" you send $500 to the jackpot." << std::endl;
	int casino_dice_roll{ dice_roller() };
	if (casino_dice_roll & 1) {
		std::cout << "You're lucky! You gain $500!" << std::endl;
		board_data->change_cash(i, 500);
	} else {
		std::cout << "You're not lucky this time. You send $500 to the jackpot." << std::endl;
		board_data->change_cash(i, -500);
		board_data->change_jackpot(500);
	}
}

void walk_for_charity::land_action(int i) {
	std::cout << "Walk for charity: You go for a walk to raise money for charity.\n"
		"Roll a die to see how far you manage to walk.\nAll other players donate"
		" 100 * your roll to the charity (jackpot)." << std::endl;
	int walk_roll{ dice_roller() };
	for (int j{}; j < board_data->get_number_of_players(); j++) {
		if (i != j) {
			board_data->change_cash(j, (-walk_roll * 100));
		}
	}
	board_data->change_jackpot((board_data->get_number_of_players() - 1) * 100 * walk_roll);
}