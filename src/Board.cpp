#include "Board.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>

/*!
 * \brief Functions used to output/display the board.
 */

// A template function to return the number of digits of an integer or double.
template <class c_type> c_type number_of_digits(c_type number)
{
	return (number < 10 ? 1 :
		(number < 100 ? 2 :
			(number < 1000 ? 3 :
				(number < 10000 ? 4 :
					(number < 100000 ? 5 :
						(number < 1000000 ? 6 :
							(number < 10000000 ? 7 :
								(number < 100000000 ? 8 :
									(number < 1000000000 ? 9 :
										10)))))))));
}

// Overloads the << operator for the board class.
std::ostream & operator<<(std::ostream & os, const board & b)
{
	std::string row_1{ "|            |             |             |              |             |                    |             |" };
	std::string row_2{ "|            |             |             |              |             |                    |             |" };
	std::string row_3{ "|            |             |             |              |             |                    |             |" };
	std::string row_4{ "|            |             |             |              |             |                    |             |" };
	std::string row_5{ "|            |             |             |                                                               |" };
	std::vector<std::string> player_characters{ "R", "B", "G", "Y" };
	for (int i{}; i < b.number_of_players; i++) {
		if (1 == b.position[i]) row_1.replace(13 + + b.player_number[i], 1, player_characters[i]);
		if (2 == b.position[i]) row_1.replace(27 + b.player_number[i], 1, player_characters[i]);
		if (3 == b.position[i]) row_1.replace(41 + b.player_number[i], 1, player_characters[i]);
		if (4 == b.position[i]) row_1.replace(56 + b.player_number[i], 1, player_characters[i]);
		if (5 == b.position[i]) row_1.replace(70 + b.player_number[i], 1, player_characters[i]);
		if (6 == b.position[i]) row_1.replace(91 + b.position[i] + b.player_number[i], 1, player_characters[i]);

		if (7 == b.position[i]) row_2.replace(0 + b.player_number[i], 1, player_characters[i]);
		if (8 == b.position[i]) row_2.replace(13 + b.player_number[i], 1, player_characters[i]);
		if (9 == b.position[i]) row_2.replace(27 + b.player_number[i], 1, player_characters[i]);
		if (10 == b.position[i]) row_2.replace(41 + b.player_number[i], 1, player_characters[i]);
		if (11 == b.position[i]) row_2.replace(56 + b.player_number[i] - 1, 1, player_characters[i]);
		if (12 == b.position[i]) row_2.replace(70 + b.player_number[i] - 1, 1, player_characters[i]);
		if (13 == b.position[i]) row_2.replace(91 + b.player_number[i] - 1, 1, player_characters[i]);

		if (14 == b.position[i]) row_3.replace(0 + b.player_number[i], 1, player_characters[i]);
		if (15 == b.position[i]) row_3.replace(13 + b.player_number[i], 1, player_characters[i]);
		if (16 == b.position[i]) row_3.replace(27 + b.player_number[i], 1, player_characters[i]);
		if (17 == b.position[i]) row_3.replace(41 + b.player_number[i], 1, player_characters[i]);
		if (18 == b.position[i]) row_3.replace(56 + b.player_number[i], 1, player_characters[i]);
		if (19 == b.position[i]) row_3.replace(70 + b.player_number[i], 1, player_characters[i]);
		if (20 == b.position[i]) row_3.replace(91 + b.player_number[i], 1, player_characters[i]);

		if (21 == b.position[i]) row_4.replace(0 + b.player_number[i], 1, player_characters[i]);
		if (22 == b.position[i]) row_4.replace(13 + b.player_number[i], 1, player_characters[i]);
		if (23 == b.position[i]) row_4.replace(27 + b.player_number[i], 1, player_characters[i]);
		if (24 == b.position[i]) row_4.replace(41 + b.player_number[i], 1, player_characters[i]);
		if (25 == b.position[i]) row_4.replace(56 + b.player_number[i], 1, player_characters[i]);
		if (26 == b.position[i]) row_4.replace(70 + b.player_number[i], 1, player_characters[i]);
		if (27 == b.position[i]) row_4.replace(91 + b.player_number[i], 1, player_characters[i]);

		if (28 == b.position[i]) row_5.replace(0 + b.player_number[i], 1, player_characters[i]);
		if (29 == b.position[i]) row_5.replace(13 + b.player_number[i], 1, player_characters[i]);
		if (30 == b.position[i]) row_5.replace(27 + b.player_number[i], 1, player_characters[i]);
		if (31 == b.position[i]) row_5.replace(41 + b.player_number[i], 1, player_characters[i]);
	};
	double jackpot_digits{};
	jackpot_digits = number_of_digits<double>(b.jackpot);
	std::string jackpot_spaces_output;
	for (int i{}; i < (25 - jackpot_digits); i++) jackpot_spaces_output.append(" ");
	std::cout <<
		" ________________________________________________________________________________________________________ \n"
		"|Sunday      |Monday       |Tuesday      |Wednesday     |Thursday     |Friday              |Saturday     |\n"
		"|____________|_____________|_____________|______________|_____________|____________________|_____________|\n"
		"|START       |Mail 1       |Sweepstakes  |Mail 2        |Deal!        |Mail 2              |Lottery      |\n"
		"|    --->    |1            |2            |3             |4            |5                   |6            |\n"
		<< row_1 <<
		"\n|____________|_____________|_____________|______________|_____________|____________________|_____________|\n"
		"|Sweet Sunday|Radio Contest|Found a Buyer|Happy Birthday|Mail 1       |Deal!               |Found A Buyer|\n"
		"|7           |8            |9            |10            |11           |12                  |13           |\n"
		<< row_2 <<
		"\n|____________|_____________|_____________|______________|_____________|____________________|_____________|\n"
		"|Fun Day     |Deal!        |Mail 3       |Found a Buyer |Buy Groceries|Mail 1              |Lottery      |\n"
		"|14          |15           |16           |17            |18           |19                  |20           |\n"
		<< row_3 <<
		"\n|____________|_____________|_____________|______________|_____________|____________________|_____________|\n"
		"|What a Deal!|Mail 1       |Found a Buyer|Mail 2        |Deal!        |Family Casino Night!|Found A Buyer|\n"
		"|21          |22           |23           |24            |25           |26                  |27           |\n"
		<< row_4 <<
		"\n|____________|_____________|_____________|______________|_____________|____________________|_____________|\n"
		"|Charity Walk|Found a Buyer|Found a Buyer|PAY DAY                      Jackpot:$" << b.jackpot <<
		jackpot_spaces_output
		<< "|\n" 
		"|28          |29           |30           |31                                                             |\n"
		<< row_5 <<
		"\n|____________|_____________|_____________|_______________________________________________________________|"
		<< std::endl;
	return os;
}