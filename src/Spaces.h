#ifndef SPACES_H
#define SPACES_H

#include "Board.h"
#include <iostream>
#include <string>
#include <vector>

/*!
 * \class spaces
 *
 * \brief Abstract base class for board spaces.
 */
class spaces
{
public:
	// Virtual destructor
	virtual ~spaces() {}
	// Dice rolling function
	int dice_roller();
	// Pure virtual functions
	virtual void card_land_action(int i, std::string description, int cost, int value) = 0;
	virtual void land_action(int i) = 0;
	virtual int const get_date() = 0;
};

// Abstract derived class for board spaces that use cards.
class card_spaces : public spaces
{
protected:
	board* board_data;
public:
	card_spaces() = default;
	card_spaces(board* b) :board_data{ b } {}
	void land_action(int i) {}
	// Virtual destructor
	virtual ~card_spaces() {}
};

class got_mail :public card_spaces
{
private:
	int date;
	int number_of_cards;
	// Static integer accessible by all got_mail objects.
	// Used as a key to access data of the deck of mail cards.
	static int mail_card_counter;
public:
	got_mail() = default;
	got_mail(board* b, int d, int n) : card_spaces{ b }, date{ d }, number_of_cards{ n } {}
	~got_mail() {}
	void card_land_action(int i, std::string description, int cost, int value);
	int const get_date() { return date; }
	int const get_number_of_cards() { return number_of_cards; }
	int const get_mail_card_counter() { return mail_card_counter; }
	void reset_mail_card_counter() { mail_card_counter = 0; }
};

class got_deal :public card_spaces
{
private:
	int date;
public:
	got_deal() = default;
	got_deal(board* b, int d) :
		card_spaces{ b }, date{ d } {}
	~got_deal() {}
	void card_land_action(int i, std::string description, int cost, int value);
	int const get_date() { return date; }
};

class special_deal :public card_spaces
{
private:
	int date;
public:
	special_deal() = default;
	special_deal(board* b, int d) : card_spaces{ b }, date{ d } {};
	~special_deal() {}
	void card_land_action(int i, std::string description, int cost, int value);
	int const get_date() { return date; }
};

// Abstract derived class for board spaces that don't use cards

class regular_spaces :public spaces
{
protected:
	board* board_data;
public:
	regular_spaces() = default;
	regular_spaces(board* b) : board_data{ b } {}
	void card_land_action(int i, std::string description, int cost, int value) {}
	// Virtual destructor
	virtual ~regular_spaces() {}
};

class found_a_buyer :public regular_spaces
{
private:
	int date;
public:
	found_a_buyer() = default;
	found_a_buyer(board* b, int d) : regular_spaces{ b }, date{ d } {};
	~found_a_buyer() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class payday_space :public regular_spaces
{
private:
	int date;
public:
	payday_space() = default;
	payday_space(board* b, int d) :
		regular_spaces{ b }, date{ d } {};
	~payday_space() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class sweepstakes : public regular_spaces
{
private:
	int date;
public:
	sweepstakes() = default;
	sweepstakes(board* b, int d) : regular_spaces{ b }, date{ d } {}
	~sweepstakes() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class lottery :public regular_spaces
{
private:
	int date;
public:
	lottery() = default;
	lottery(board * b, int d) : regular_spaces{ b }, date{ d } {}
	~lottery() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class sweet_sunday :public regular_spaces
{
private:
	int date;
public:
	sweet_sunday() = default;
	sweet_sunday(board * b, int d) : regular_spaces{ b }, date{ d } {}
	~sweet_sunday() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class radio_contest :public regular_spaces
{
private:
	int date;
public:
	radio_contest() = default;
	radio_contest(board * b, int d) : regular_spaces{ b }, date{ d } {}
	~radio_contest() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class happy_birthday : public regular_spaces
{
private:
	int date;
public:
	happy_birthday() = default;
	happy_birthday(board * b, int d) : regular_spaces{ b }, date{ d } {}
	~happy_birthday() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class fun_day :public regular_spaces
{
private:
	int date;
public:
	fun_day() = default;
	fun_day(board * b, int d) : regular_spaces{ b }, date{ d } {}
	~fun_day() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class buy_groceries :public regular_spaces
{
private:
	int date;
public:
	buy_groceries() = default;
	buy_groceries(board * b, int d) : regular_spaces{ b }, date{ d } {}
	~buy_groceries() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class date_night_at_the_casino :public regular_spaces
{
private:
	int date;
public:
	date_night_at_the_casino() = default;
	date_night_at_the_casino(board * b, int d) : regular_spaces{ b }, date{ d } {}
	~date_night_at_the_casino() {}
	void land_action(int i);
	int const get_date() { return date; }
};

class walk_for_charity :public regular_spaces
{
private:
	int date;
public:
	walk_for_charity() = default;
	walk_for_charity(board * b, int d) : regular_spaces{ b }, date{ d } {}
	~walk_for_charity() {}
	void land_action(int i);
	int const get_date() { return date; }
};
#endif