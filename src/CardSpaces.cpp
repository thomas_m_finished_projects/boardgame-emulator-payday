#include "Cards.h"
#include "Board.h"
#include "Spaces.h"
#include <iostream>
#include <string>
#include <vector>

/*!
 * \brief Contains functions used to carry out instructions for card spaces.
 */

//Static class data for counters
int got_mail::mail_card_counter{};

// Function to display information on mail cards, and change player data accordingly.
void got_mail::card_land_action(int i, std::string descriptions, int cost, int value)
{
    std::string description{ descriptions };
    std::string type_check{ description[0] };
    std::vector<std::string> printing_vector;
    // Uses a delimiter based on the format of the data in the MailCards.dat file.
    std::string delimiter = "_";
    size_t pos = 0;
    while ((pos = description.find(delimiter)) != std::string::npos) {
        printing_vector.push_back(description.substr(0, pos));
        description.erase(0, pos + delimiter.length());
    }
    printing_vector.push_back(description.substr(0, pos));
    for (int j{}; j < printing_vector.size(); j++) { std::cout << printing_vector[j] << std::endl; }
    if (type_check == "B") {
        std::cout << "Pay $" << cost << "." << std::endl;
        board_data->change_bills(i, cost);
    }
    if (type_check == "T") {
        std::cout << "You send $" << cost << " to the jackpot." << std::endl;
        board_data->change_jackpot(cost);
        board_data->change_cash(i, -cost);
    }
    // Updates the mail card counter so the next mail card in the deck is accessed next time the
    // counter is used.
    mail_card_counter = mail_card_counter + 1;
    printing_vector.clear();
}

// Function to display information on deal cards, check if the user would like to purchase it and
// update information accordingly.
void got_deal::card_land_action(int i, std::string input_descriptions, int input_cost, int input_value)
{
    std::cout << "You have recieved the offer of a deal!\nPlease type 'Y' if you would like to purchase it." << std::endl;
    std::string description{ input_descriptions };
    std::string full_description{description};
    std::vector<std::string> printing_vector;
    std::string delimiter = "_";
    size_t pos = 0;
    while ((pos = description.find(delimiter)) != std::string::npos) {
        printing_vector.push_back(description.substr(0, pos));
        description.erase(0, pos + delimiter.length());
    }
    printing_vector.push_back(description.substr(0, pos));
    std::cout << "Cost: $" << input_cost << "\n" << "Value: $" << input_value << "." << std::endl;
    for (int j{}; j < printing_vector.size(); j++) { std::cout << printing_vector[j] << std::endl; }
    std::string purchase_choice;
    std::cin >> purchase_choice;
    if (purchase_choice == "Y") {
        // Charges player for the cost of the deal.
        board_data->change_cash(i, -input_cost);
        // Stores information on purchased deals in the got_deal class data.
        board_data->add_description(full_description);
        board_data->add_cost(input_cost);
        board_data->add_value(input_value);
        board_data->add_ownership(i);
        board_data->increase_deal_card_counter();
        board_data->increase_number_of_deals();
    } else { board_data->increase_deal_card_counter(); }
    printing_vector.clear();
}


void special_deal::card_land_action(int i, std::string input_descriptions, int input_cost, int input_value)
{
    std::cout << "What a deal!\nYou have found a discounted deal that you can't refuse." << std::endl;
    std::cout << "The deal is yours for only $100 times your roll." << std::endl;
    std::string description{ input_descriptions };
    std::string full_description{ description };
    int dice_roll{ dice_roller() };
    int price{ dice_roll * 100 };
    std::cout << "The deal is usually $" << input_cost << ".\nIt will be yours for only $" << price << "." << std::endl;
    std::vector<std::string> printing_vector;
    std::string delimiter = "_";
    size_t pos = 0;
    while ((pos = description.find(delimiter)) != std::string::npos) {
        printing_vector.push_back(description.substr(0, pos));
        description.erase(0, pos + delimiter.length());
    }
    printing_vector.push_back(description.substr(0, pos));
    for (int j{}; j < printing_vector.size(); j++) { std::cout << printing_vector[j] << std::endl; }
    board_data->change_cash(i, -price);
    board_data->add_description(full_description);
    board_data->add_cost(price);
    board_data->add_value(input_value);
    board_data->add_ownership(i);
    board_data->increase_deal_card_counter();
    board_data->increase_number_of_deals();
    printing_vector.clear();
}

// Function to allow user to sell available deals.
void found_a_buyer::land_action(int i)
{
    if (board_data->get_number_of_deals() == 0) {
        std::cout << "There are no available deals.\nYou decide to take the day off to rest." << std::endl;
    } else {
        std::vector<std::string> descriptions_available;
        std::vector<int> values_available;
        std::vector<int> deal_number;
        // Initialises a variable used to count the number of deals owned by the relevant player.
        int is_deal_available{0};
        for (int j{}; j < board_data->get_number_of_deals(); j++) {
            if (board_data->get_ownership(j) == i) {
                is_deal_available = is_deal_available + 1;
                descriptions_available.push_back(board_data->get_description(j));
                values_available.push_back(board_data->get_values(j));
                deal_number.push_back(j);
            }
        }
        if (is_deal_available != 0) {
            std::cout << "You can now sell an owned deal." << std::endl;
            for (int k{}; k < deal_number.size(); k++) {
                std::cout << "Deal "<<(k+1)<<": " << descriptions_available[k] << "\nValue: $" <<
                    values_available[k] << std::endl;
            }
            int was_input_successful{ 1 };
            int loop_not_ending_flag{ 0 };
            while (was_input_successful == 1) {
                std::cout << "Please enter the number corresponding to a deal you would like to purchase." << std::endl;
                std::cout<<"If you would not like to sell a deal, please enter 0."<<std::endl;
                int deal_input{};
                std::cin >> deal_input;
                if (std::cin.good()) {
                    if ((deal_input<0)||(deal_input>deal_number.size())){
                        std::cout << "Please enter a number equal to 0 or corresponding to an available deal." << std::endl;
                    } else{
                        if (deal_input == 0) {
                            was_input_successful = 0;
                        } else{
                            // Updates the player's cash and calls a function to remove the sold deal
                            // from the got_deal object data.
                            was_input_successful = 0;
                            board_data->change_cash(i, values_available[deal_input - 1]);
                            int deal_to_be_removed{ deal_number[deal_input - 1] };
                            board_data->remove_a_deal(deal_to_be_removed);
                        }
                    }
                } else { std::cout << "Please enter a valid integer." << std::endl; }

            }
        } else { std::cout << "You have no available deals.\nYou decide to take the day off to rest." << std::endl; }
    }
}

// Removes information corresponding to a previously available deal, in the got_deal object.
void board::remove_a_deal(int i)
{
    //i is the element in the deal vectors to be removed.
    number_of_deals = number_of_deals - 1;
    descriptions.erase(descriptions.begin() + i);
    costs.erase(costs.begin() + i);
    values.erase(values.begin() + i);
    ownership.erase(ownership.begin() + i);
}