#include "Cards.h"
#include "Board.h"
#include "Spaces.h"
#include <iostream>
#include <string>
#include <vector>


// Function to carry out actions requires when a player lands on the payday space.
void payday_space::land_action(int i)
{
    std::cout << "You reached the end of the month.\n\nIt's PAYDAY!\nFirstly, you collect your monthly salary" <<
        " of $3500."<<std::endl;
    board_data->change_cash(i, 3500);
    std::cout << "Your current loan is: $" << board_data->get_player_loan(i) << ".\nYou are charged 10% interest "
        "which is removed from your current cash." << std::endl;
    board_data->change_cash(i, (0.1 * board_data->get_player_loan(i)));
    // Prompts the user to pay off their loan.
    if (board_data->get_player_loan(i) > 0) {
        std::cout << "If you wish to, you can pay off part or all of your loan.\nPlease enter the amount you would like "
            "to pay off (entering 0 if you wouldn't like to pay).\nPayments must be in $1000 increments." << std::endl;
        int loan_input_success{ 1 };
        int loop_not_ending_flag{ 0 };
        // Uses a loop to ensure user input is of a valid type and value.
        while (loan_input_success == 1) {
            int loan_repayment{};
            loop_not_ending_flag = loop_not_ending_flag + 1;
            std::cin >> loan_repayment;
            if (std::cin.good()) {
                if ((loan_repayment == 0) || (loan_repayment % 1000 == 0)) {
                    if (loan_repayment <= board_data->get_player_loan(i)) {
                        if (loan_repayment <= board_data->get_player_cash(i)) {
                            loan_input_success = 0;
                            board_data->change_cash(i, -loan_repayment);
                            board_data->change_loan(i, -loan_repayment);

                        }
                        else { std::cout << "You don't have the cash pay off that much of you loan." << std::endl; }
                    }
                    else { std::cout << "You can't repay more than your maximum loan." << std::endl; }
                }
                else { std::cout << "Please enter a valid amount to repay" << std::endl; }
            }
            else { std::cout << "Please enter a valid integer input." << std::endl; }
            if (loop_not_ending_flag == 30) {
                loan_input_success = 0;
                std::cerr << "Loan repayment error:\nLoan unable to be repaid." << std::endl;
            }
        }

    }
    std::cout << "Now you must repay all the bills you recieved this month: $" <<
        board_data->get_player_bills(i) << "." << std::endl;
    board_data->change_cash(i, (-board_data->get_player_bills(i)));
    board_data->loan_check_and_take_out(i);
    // Increases the player month count and moves them back to space 1.
    board_data->advance_a_month(i);
    if (board_data->get_player_months(i) == (board_data->get_total_number_of_months())) {
        // Moves the player back to the end of the month.
        board_data->manually_change_position(i, 31);
        std::cout << "\n" << board_data->get_player_name(i) << " player, you have completed the last month of play." << std::endl;
        std::cout << "Your current cash is: $" << board_data->get_player_cash(i) << ".\nYour current loan is: $"
            << board_data->get_player_loan(i) << ".\nSubtracing any outstanding loans, your net worth is $";
        board_data->change_cash(i, -(board_data->get_player_loan(i)));
        board_data->change_loan(i, -(board_data->get_player_loan(i)));
        std::cout << board_data->get_player_cash(i) << "." << std::endl;
    }
}