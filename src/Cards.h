#ifndef CARDS_H
#define CARDS_H

#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <tuple>

// Definitions of maps for deal and map cards.
typedef std::map<int, std::string> deal_cards;
typedef std::map<int, std::string> mail_cards;

// Definitions of functions to return information from the deal and mail card maps.
std::tuple<std::string, int> get_mail_card_information(mail_cards& mail, int selector);
std::tuple<std::string, int, int> get_deal_card_information(deal_cards& mail, int selector);

#endif