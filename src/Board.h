#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <string>
#include <vector>

// Definitions of functions used to read and write from / to files.
void read_from_file(std::vector<std::string>& data_vector, std::string data_filename);
void update_and_output_leaderboard(std::vector<std::string> leaderboard_vector,std::string leaderboard_filename,
	std::vector<std::string> player_names, std::vector<int> is_player_winner, std::vector<double> end_game_net_worth);

// Definitions of functions used to take user inputs.
int input_number_of_players();
int input_number_of_months();

/*!
 * \class board
 *
 * \brief Class used to store, modify, access and print
 * the board / the player data.
 * Contains the player / piece data.
 */

class board
{
	// Friend function to overload << operator.
	friend std::ostream& operator<<(std::ostream& os, const board& b);
private:
	double jackpot{};
	size_t days{ 31 };
	int number_of_players{};
	std::vector<size_t> position;
	std::vector<int> player_number;
	std::vector<std::string> player_name;
	std::vector<double> cash;
	std::vector<double> loan;
	std::vector<double> bills;
	std::vector<int> months;
	int number_of_deals;
	std::vector<std::string> descriptions;
	std::vector<int> costs;
	std::vector<int> values;
	std::vector<int> ownership;
	int deal_card_counter;
	int total_number_of_months;
public:
	// Default constructor
	board() = default;
	// Parametised constructor
	board(double j, size_t d, int n, std::vector<size_t> p, std::vector<int> nu,
		std::vector<std::string> na, std::vector<double> c, std::vector<double> l, std::vector<double> b,
		std::vector<int> m,
		int nd, std::vector<std::string> desc, std::vector<int>cos,std::vector<int>val,std::vector<int>own,int count,int tmon):
		jackpot{j}, days{ d }, number_of_players{ n }, position{ p }, player_number{ nu },
		player_name{ na }, cash{ c }, loan{ l }, bills{ b }, months{ m },
		number_of_deals{ nd }, descriptions{ desc }, costs{ cos }, values{ val }, ownership{ own }, deal_card_counter{ count }
		,total_number_of_months{ tmon } {}
	// Default destructor
	~board() {}
	// Functions to return board/player data.
	double const& get_jackpot() { return jackpot; }
	size_t const& get_board_length() { return days; }
	int const& get_number_of_players() { return number_of_players; }
	size_t const& get_player_position(int i) { return position[i]; }
	int const& get_player_number(int i) { return player_number[i]; }
	std::string const& get_player_name(int i) { return player_name[i]; }
	double const& get_player_cash(int i) { return cash[i]; }
	double const& get_player_loan(int i) { return loan[i]; }
	double const& get_player_bills(int i) { return bills[i]; }
	int const& get_player_months(int i) { return months[i]; }
	int const& get_total_number_of_months() { return total_number_of_months; }
	// Functions to modify board/player data.
	void output_player_information(int i);
	void change_loan(int i, double l);
	void change_cash(int i, double c);
	void change_position(int i);
	void manually_change_position(int i, int new_position);
	void change_jackpot(double j);
	void change_bills(int i, double b);
	void advance_a_month(int i);
	void loan_check_and_take_out(int i);
	//Functions involving deal card data.
	std::string const get_description(int i) { return descriptions[i]; }
	int const get_cost(int i) { return costs[i]; }
	int const get_values(int i) { return values[i]; }
	int const get_ownership(int i) { return ownership[i]; }
	int const get_number_of_deals() { return number_of_deals; }
	// Function to remove an element from each of the vectors containing information on owned deals.
	void remove_a_deal(int i);
	// Functions used by the to update information on available deals.
	int const get_deal_card_counter() { return deal_card_counter; }
	void reset_deal_card_counter() { deal_card_counter = 0; }
	void add_description(std::string desc) { descriptions.push_back(desc); }
	void add_cost(int cost) { costs.push_back(cost); }
	void add_value(int value) { values.push_back(value); }
	void add_ownership(int owner) { ownership.push_back(owner); }
	void increase_deal_card_counter() { deal_card_counter = deal_card_counter + 1; }
	void increase_number_of_deals() { number_of_deals = number_of_deals + 1; }
};

#endif