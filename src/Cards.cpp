#include "Cards.h"
#include "Board.h"
#include "Spaces.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <map>
#include <fstream>
#include <tuple>

/*!
 * \brief Contains functions used to generate tuples from map data.
 */

// Function to return a tuple containing data from an element in a mail_cards map.
std::tuple<std::string, int> get_mail_card_information(mail_cards& mail, int selector)
{
    // Uses an iterator to access an element in the mail map.
    mail_cards::iterator mail_iter;
    mail_iter = mail.find(selector);
    if (mail_iter != mail.end()) {
        std::string total_card_information = mail_iter->second;
        std::string delimiter = "/"; // Uses a delimiter to split up the string in the pair.
        size_t pos = 0;
        std::vector<std::string> information_vector;
        while ((pos = total_card_information.find(delimiter)) != std::string::npos) {
            information_vector.push_back(total_card_information.substr(0, pos));
            total_card_information.erase(0, pos + delimiter.length());
        }
        information_vector.push_back(total_card_information.substr(0, pos));
        // Returns tuples containing the description and numerical value of the mail card data.
        // If the tuple has no numerical data, 0 is returned.
        if (information_vector.size() == 1) {
            return std::make_tuple(information_vector[0], 0);
        }
        if (information_vector.size() == 2) {
            int amount{ std::stoi(information_vector[1]) };
            return std::make_tuple(information_vector[0], amount);
        }
        else {
            std::cerr << "Vector size error, card data does not match formatted design." << std::endl;
            return std::make_tuple("Vector size error", 0); }
    } else {
        std::cerr << "Card selection error, mail card " << selector << " is not an available card." << std::endl;
        return std::make_tuple("Card selection error", 0); }
}

// Function to return a tuple containing data from an element in a deal_cards map.
std::tuple<std::string, int, int> get_deal_card_information(deal_cards& mail, int selector)
{
    deal_cards::iterator deal_iter;
    deal_iter = mail.find(selector);
    if (deal_iter != mail.end()) {
        std::string total_card_information = deal_iter->second;
        std::string delimiter = "/";
        size_t pos = 0;
        std::vector<std::string> information_vector;
        while ((pos = total_card_information.find(delimiter)) != std::string::npos) {
            information_vector.push_back(total_card_information.substr(0, pos));
            total_card_information.erase(0, pos + delimiter.length());
        }
        information_vector.push_back(total_card_information.substr(0, pos));
        if (information_vector.size() == 3) {
            int cost{ std::stoi(information_vector[1]) };
            int value{ std::stoi(information_vector[2]) };
            return std::make_tuple(information_vector[0], cost, value);
        }
        else {
            std::cerr << "Vector size error, card data does not match formatted design." << std::endl;
            return std::make_tuple("Vector size error", 0, 0);
        }
    }
    else {
        std::cerr << "Card selection error, mail card " << selector << " is not an available card." << std::endl;
        return std::make_tuple("Card selection error", 0, 0);
    }
}